package android.aut.hu.weardemo;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;

public class OutgoingCallListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

            // Build intent for open on phone
            Intent viewIntent = new Intent(context, MainActivity.class);
            PendingIntent viewPendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);

            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle("Outgoing call")
                            .setContentText(phoneNumber)
                            .setContentIntent(viewPendingIntent)
                            .setAutoCancel(true);

            //Voice reply part
            String replyLabel = "Please reply";
            String[] replyChoices = context.getResources().getStringArray(R.array.reply_choices);

            RemoteInput remoteInput = new RemoteInput.Builder(MainActivity.EXTRA_VOICE_REPLY)
                    .setLabel(replyLabel)
                    .setChoices(replyChoices)
                    .build();

            NotificationCompat.Action action = new NotificationCompat.Action.Builder(R.drawable.ic_launcher, "Reply", viewPendingIntent)
                    .addRemoteInput(remoteInput)
                    .build();

            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
            NotificationCompat.WearableExtender wearableExtender =
                    new NotificationCompat.WearableExtender()
                            .setHintHideIcon(true)
                            .setBackground(bitmap)
                            .addAction(action);

            notificationBuilder.extend(wearableExtender);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(1, notificationBuilder.build());
        }
    }
}
