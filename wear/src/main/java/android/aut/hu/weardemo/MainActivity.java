package android.aut.hu.weardemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, MessageApi.MessageListener {

    public static final String QUERY_FLASH_ON = "/request/on";
    public static final String QUERY_FLASH_OFF = "/request/off";
    public static final String QUERY_RESPONSE_PATH = "/response";

    private GoogleApiClient mGoogleApiClient;
    private Node mNode;
    private boolean mResolvingError = false;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //We use WatchViewStub so need to wait for views
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                final ToggleButton btnToggleFlash = (ToggleButton) stub.findViewById(R.id.btnToggleFlash);
                btnToggleFlash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendFlashLightRequestMessage(btnToggleFlash.isChecked());
                    }
                });
            }
        });

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Client connected, find handheld node
        resolveNode();
        //Add data listener
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
    }

    private void resolveNode() {
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    //First node is enough
                    mNode = node;
                    break;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Connect if client is disconnected
        if (!mResolvingError && !mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            //Disconnect data listener
            Wearable.MessageApi.removeListener(mGoogleApiClient, this);
            //Disconnect client
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Not important now
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Not important now
    }

    private void sendFlashLightRequestMessage(boolean requestOn) {
        //Send message to handheld iff client is connected
        if (mNode != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            String requestPath = requestOn ? QUERY_FLASH_ON : QUERY_FLASH_OFF;

            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), requestPath,  null).setResultCallback(
                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Toast.makeText(getApplicationContext(), "Failed to send message with status code: " + sendMessageResult.getStatus().getStatusCode(), Toast.LENGTH_LONG).show();
                            }
                            else {
                                //Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
            );
        } else {
            Toast.makeText(getApplicationContext(), "Not connected to wear", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        /*
         * Receive the message from mobile
         */
        if (QUERY_RESPONSE_PATH.equals(messageEvent.getPath())) {
            final byte[] data = messageEvent.getData();

            //Show message
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), new String(data), Toast.LENGTH_LONG).show();
                }
            });
        }

    }
}
